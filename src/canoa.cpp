#include "canoa.hpp"
#include "embarcacao.hpp"
#include <iostream>
#include <string>

using namespace std;

Canoa::Canoa(){
  cout << "Canoa adicionada na posição" << endl;
}

Canoa::Canoa(int cooX, int cooY){
  this->cooX = cooX;
  this->cooY = cooY;
  cout << "Uma canoa foi adicionada com sucesso!" << endl;
}

Canoa::~Canoa(){
    cout << "Uma canoa inimiga foi totalmente destruída!" << endl;
}

void Canoa::skill(){
  cout << "Esta embarcação não possui nenhuma habilidade!" << endl;
}

void Canoa::set_name_emb(string name_emb){
  this->name_emb = name_emb;
}

void Canoa::set_life(int life){
  this->life += life;
}

void Canoa::set_casas(int casas){
  this->casas += casas;
}

int Canoa::to_suffer_damage(int x, int y){
  //Quando a canoa sofre dano ela é totalmente destruída e chamamos o seu método destrutor
  set_life(-1);
  set_casas(-1);
}
