#include "draw.hpp"
#include <iostream>
#include <cstdlib>
#include <string>
#include <dirent.h>
#include <fstream>
#include <utility>
#include <cstdio>

using namespace std;

Draw::Draw(){
  cout << "Jogo iniciado!" << endl;
  srand((unsigned)time(0)); //Gera número aleatórios positivos
}

Draw::~Draw(){
  cout << "Jogo finalizado!" << endl;
  for (int i = 0; i < 13; i++) {
    free(barcos_aux[i]); //Desaloca linhas
  }
  free(barcos_aux); //Desaloca colunas

  for (int i = 0; i < 13; i++) {
    free(barcos_aux2[i]); //Desaloca linhas
  }
  free(barcos_aux2); //Desaloca colunas

}

void Draw::initializer(){
printf("WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW\n");
printf("W....:::::::WWWWWWW:.::::.:+WW.....::::::W.....::::::W.::::::WWWWW.::::::..:WWWWWW,,.....WWWWW.:::..*W.::...WW ::::::WW .....::::WWWWW\n");
printf("W,         ,+WWWWWW         WW,         .W,         *W       WWWWW          WWWWW         WWWW      WW     :WW      .WW,         .WWWW\n");
printf("W             WWWWW         WW          :W          *W       WWWWW          WWWW          WWW,      WW     +WW      +WW,          ,+WW\n");
printf("W              WWWW         WW          :W          *W       WWWWW          WWW            WW,      WW     :WW      +WW,            :W\n");
printf("W      WW:     WWW*         WW          :W          #W       WWWWW          WW,            WW,      WW     +WW      +WW,            .W\n");
printf("W      WW:     WWW     *     WWWW      W@W#W       :WWW      WWWWW      W@##WW      #W      WW,     WW     +WW      +WW,    ,#W,    :W\n");
printf("W      +W:     WWW    +@,    WWWW      WWWWW       WWW       WWWWW      WWWWWW      *W      WW      WW     +WW      +WW,    ,#W,    :W\n");
printf("W      +W:     WWW    +W     WWWW      WWWWW       WWW       WWWWW      WWWWWW      *W      WW      WW     *WW      +WW,    ,#W,    +W\n");
printf("W      +W:     WWW    +W     WWWW      WWWWW       WWW       WWWWW      WWWWWW      *W      WW      WW     *WW      +WW,    .#W.    +W\n");
printf("W      +W:    .WWW   :+W:    WWWW      WWWWW       WWW       WWWWW      ####WW      *WWWWWWWWW,     WW     *WW      *WW,    .#W.    +W\n");
printf("W      ,,  ::+#WWW   :*W:    WWWW      WWWWW       WWW       WWWWW          WW:         +WWWWW,            *WW      *WW,    :#W.    *W\n");
printf("W           WWWWW.   *W::    WWWW      WWWWW       WWW       WWWWW          WW+          +WWWW,            *WW      *WW,    .#W.    *W\n");
printf("W            WWWWW          +WWWW      WWWWW       WWW       WWWWW          WWW#         .+WWW,            *WW      *WW,            +W\n");
printf("W             WWW#           WWWW      WWWWW       WWW       WWWWW          WWWW*:        .+WW.            #WW      *WW,            @W\n");
printf("W              WW            *WWW      WWWWW       WWW       WWWWW      WWWWWWWWW#:+++     :@W:     ###    #WW      #WW,          +@WW\n");
printf("W     :*W*  ***WW             WWW      WWWWW       WWW       WWWWW      WWWWWWWWWWW@@@.    +#W:     #W:    #WW      #WW,          @WWW\n");
printf("W     ++W*   *@WW             WWW      WWWWW       WWW       WWWWW      WWWWWWWWWWWWWW.    +#W:     *W:    #WW      #WW,     *@@@WWWWW\n");
printf("W:    :+W*   *@WW    #####    WWW      WWWWW       WWW       WWWWW      WWWWWW::*+***W.    *@W+     #W:    #WW      #WW.    :#WWWWWWWW\n");
printf("W:    ++W#    @WW:   ##***   @WWW      WWWWW       WWW       WWWWW      WWWWWW+     *W+    *#W+     #W+    #WW      #WW.    :#WWWWWWWW\n");
printf("W+    :+W*    @WW:   #***#   #WWW      WWWWW       WWW       WWWWW      WWWWWW:     #W:    *@W+     #W+    @WW      #WW.    :#WWWWWWWW\n");
printf("W:    :..,    @WW:   .@@#:   *@WW      WWWWW       WWW           W      ++++@W      *.     :WW:     *W:    @WW      #WW.    :#WWWWWWWW\n");
printf("W:    #*#    *@WW.   +WWW:   :*WW      WWWWW       WWW           W          #W             #WW:     *W:    #WW      #WW.    +#WWWWWWWW\n");
printf("W:    **     @WW++   *WWW+    *WW      WWWWW       WWW           W          #WWW          +WWW.     *W:    #WW      #WW.    :#WWWWWWWW\n");
printf("W+         *#WWW.*   *WWW+    +WW      WWWWW       WWW           W          *WWW#        +WWWW.     *W.    #WW      #WW.    :#WWWWWWWW\n");
printf("W+##******##WWWW:#   #WWW:    +WW#@@@@@WWWWW+##**#+WWW:@@@@@@@@@#W*@@@@##*#*#WWWWW*#####@WWWWW.###***W.#####WW.*####*WW.+****#WWWWWWWW\n");
printf("WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW\n");

  menu();

}

/*
void Draw::set_option(int option){
  this->option = option;
}*/

void Draw::menu(){
  int option = 0;

  cout << "Seja bem-vindo! Este é um jogo de batalha naval single player desenvolvido por  Maicon Mares." << endl;

  while (true) {
    cout << "Escolha uma opção: " << endl;
    cout << "1-Iniciar uma nova partida" << endl;
    cout << "2-Ver instruções de como jogar" << endl;
    cout << "3-Sair do jogo" << endl;
    cin >> option;

    system("clear || cls");

    if (option == 1 || option == 2 || option == 3) {
      switch (option) {
        case 1:
          cout << "Iniciando uma nova partida..." << endl;
            engine();
          break;
        case 2:
          cout << "Como jogar: " << endl;
          cout << "Este é um clássico jogo de Batalha Naval em que para você ganhar é necessário destruir todas as embarcações." << endl;
          cout << "Porém, algumas embarcações possuem habilidades especiais. Temos 3 tipos de embarcações:" << endl;
          cout << "-Canoa: não possui nenhuma habilidade e ocupa somente uma casa, apenas um tiro a destrói completamente." << endl;
          cout << "-Submarino: devido as enormes profundidades que ele consegue alcançar, ele possui a habilidade de resistência e ocupa 2 casas."<< endl;
          cout << "É necessário dois mísseis para destruir cada casa. Ou seja, você precisará de acertar 4 tiros e cada par de tiros em uma casa diferente." << endl;
          cout << "-Porta Aviões: é uma enorme embarcação que ocupa 4 casas." << endl;
          cout << "Parece ser fácil alvejá-la, entretanto ela possui a habilidade de abater mísseis ainda no ar!" << endl;
          cout << "Para vencer você terá que alcançar um mínimo de 52 pontos." << endl;
          cout << "Pontuação por tipo de embarcação destruída:" << endl;
          cout << "Canoa: 2 pontos." << endl;
          cout << "Submarino: 6 pontos." << endl;
          cout << "Porta aviões: 8 pontos." << endl;
          break;
        case 3:
          cout << "Até mais tarde!" << endl;
          exit(0);
          break;
      }
    }  else {
        cout << "Esta opção não existe! Tente novamente!" << endl;
      }
    }
}

void Draw::engine(){
  int choice_map, choice_enemy;
  string name_map;
  //Adicionar a opção de criar o próprio mapa para jogar
  //Adicionar a opção de escolher jogar contra a máquina ou contra um player real
  cout << "Escolha o modo de jogo: " << endl;
  cout << "1-Jogador vs computador" << endl;
  cout << "2-Player vs player" << endl;
  cin >> choice_enemy;
  if (choice_enemy == 1) {
    cout << "Qual é o seu nome? ";
    cin >> players.first;
    player_vs_machine();
  } else if (choice_enemy == 2) {
    cout << "Qual é o seu nome? Player 1: ";
    cin >> players.first;
    cout << "Qual é o seu nome? Player 2: ";
    cin >> players.second;
    system("clear || cls");
    cout << "Sejam bem-vindos " << players.first << " e " << players.second << endl;
    organize();
    player_vs_player();
  }
}

//Método que mostra os mapas disponíveis
void Draw::show_dir() {
    string nomeDir;
    nomeDir = "../ep1/doc";
    //Ponteiro que aponta para o tipo diretório
    DIR *dir = 0;

    //Ponteiro que irá iterar os arquivos
    struct dirent *entrada = 0;

    //isDir indica se trata de um diretório e isFile indica se trata de um arquivo
    unsigned char isDir = 0x4;
    unsigned char isFile = 0x8;

    dir = opendir (nomeDir.c_str());

    if (dir == 0) {
        cout << "Nao foi possivel abrir diretorio." << endl;
        exit (1);
    }

    nome_mapas.clear();
    //Iterar sobre o diretorio e adicionar o nome dos mapas ao vector
    while ((entrada = readdir (dir))){
        if (entrada->d_type == isFile){
            nome_mapas.push_back(entrada->d_name);
        }
    }

    closedir (dir);

    //Printar nome dos mapas
    cout << "Nome dos mapas:" << endl;
    for (int i = 0; i < nome_mapas.size(); i++) {
      if (nome_mapas[i] != ".gitkeep") {
        cout << i << "-" << nome_mapas[i] << endl;
      }
    }

}


//Implementar a função de mostrar qualquer mapa que o jogador escolher
void Draw::show_map(int number_map){
  if (number_map == 1) {
    cout << "  ";
    for (int i = 0; i < 13; i++) {if (i > 10) {cout << "  " << i;} else cout << " " << i;}
    cout << endl;
    for (int k = 0; k < 13; k++) {
        if (k < 10) cout << k << "  ";
        else cout << k << " ";
        for (int p = 0; p < 13; p++) {
          //cout << " " << p;
          if (p < 10) cout << barcos_aux[k][p] << " ";
          else cout << barcos_aux[k][p] << "   ";
        }
      cout << endl;
    }
  } else if(number_map == 2){
    cout << "  ";
    for (int i = 0; i < 13; i++) {if (i > 10) {cout << "  " << i;} else cout << " " << i;}
    cout << endl;
    for (int k = 0; k < 13; k++) {
        if (k < 10) cout << k << "  ";
        else cout << k << " ";
        for (int p = 0; p < 13; p++) {
          //cout << " " << p;
          if (p < 10) cout << barcos_aux2[k][p] << " ";
          else cout << barcos_aux2[k][p] << "   ";
        }
      cout << endl;
    }
  }
}

void Draw::organize(){
  int option;
  pair<int, int>choice_map;
  string name_map, tecla;
  cout << players.first << " escolha um mapa!" << endl;
  cout << "Você pode escolher os seguintes mapas:" << endl;
  show_dir();
  cout << "Digite o número do mapa que deseja escolher:" << endl;
  cin >> choice_map.first;


  name_map = "./doc/";
  name_map += nome_mapas[choice_map.first];

  tabuleiro_player1(name_map);
  cout << "Deseja ver o mapa que escolheu?" << endl;
  cout << "Digite 1 para sim ou 2 para não." << endl;
  cin >> option;
  system("clear || cls");

  if (option == 1) {
    cout << "Não deixe seu adversário ver seu mapa!" << endl;
    show_map(1);
    cout << "Digite uma letra + Enter para continuar..." << endl;
    cin >> tecla;
  }
  system("clear || cls");

  tabuleiro_player2(name_map);
  cout << players.second << " deseja ver o seu mapa?" << endl;
  cout << "Digite 1 para sim ou 2 para não." << endl;
  cin >> option;

  if (option == 1) {
    cout << "Não deixe seu adversário ver seu mapa!" << endl;
    show_map(2);
    cout << "Digite uma letra + Enter para continuar..." << endl;
    cin >> tecla;
    system("clear || cls");
  }

}

void Draw::player_vs_player(){
  cout << "Agora o jogo irá começar de verdade!" << endl;
  cout << "Lembre-se que algumas embarcações possuem habilidades especiais." << endl;
  while (true) {
    cout << "----------------------------------------------------------------------" << endl;
    cout << "                            " << scores.first << "      |       " << scores.second << endl;
    cout << "                      " << players.first << "               " << players.second << endl;
    player_1();
    player_2();



    if (scores.first >= 52) {
      cout << "Parabéns! Player 1 venceu a partida" << endl;
      break;
    } else if(scores.second >= 52) {
      cout << "Parabéns! Player 2 venceu a partida" << endl;
      break;
    }
  }
}

void Draw::player_vs_machine(){
  int option;
  pair<int, int>choice_map;
  string name_map;
  cout << players.first << " escolha um mapa!" << endl;
  cout << "Você pode escolher os seguintes mapas:" << endl;
  show_dir();
  cout << "Digite o número do mapa que deseja escolher:" << endl;
  cin >> choice_map.first;


  name_map = "../ep1/doc/";
  name_map += nome_mapas[choice_map.first];

  tabuleiro_player1(name_map);
  tabuleiro_machine(name_map);
  cout << "Deseja ver o mapa que escolheu?" << endl;
  cout << "Digite 1 para sim ou 2 para não." << endl;
  cin >> option;

  if (option == 1) {
    cout << "Não deixe seu adversário ver seu mapa!" << endl;
    show_map(1);
  }
  cout << "Agora o jogo irá começar de verdade!" << endl;
  cout << "Lembre-se que algumas embarcações possuem habilidades especiais." << endl;
  while (true) {
    cout << "-------------------------------------------------------------------------------" << endl;
    cout << "                            " << scores.first << "      |       " << scores.second << endl;
    cout << "                      " << players.first << "            Máquina" << endl;
    player_1();
    machine();



    if (scores.first >= 52) {
      cout << "Parabéns! " << players.first << "venceu a partida." << endl;
      break;
    } else if(scores.second >= 52) {
      cout << "A máquina venceu a partida!" << endl;
      break;
    }
  }
}

int Draw::player_1(){
  int x, y, auxiliar;
  pair<int, int> verificar;
  pair<int,int> temp;

  //Resolver o erro de como verificar se as coordenadas que estão sendo atacadas no submarino e no porta aviões já foram atacadas antes
  cout << "----------------------------Turno de " << players.first << "------------------------" << endl;
  cout << "Digite a coordenada x: " << endl;
  cin >> x;
  cout << "\n";
  cout << "Digite a coordenada para y: " << endl;
  cin >> y;
  cout << "\n";
  if (barcos_aux2[y][x] == 'C') {
    ((Canoa *)barcos_player2[y][x])->~Canoa();
    barcos_player2[y][x] = NULL;
    scores.first += 2;
    temp.first = x;
    temp.second = y;
  } else if(barcos_aux2[y][x] == 'S') {
      verificar.first = ((Submarino *)barcos_player2[y][x])->to_suffer_damage(x, y);
      if (verificar.first == 1) {
        ((Submarino *)barcos_player2[y][x])->~Submarino();
        barcos_player2[y][x] == NULL;
        scores.first += 6;
        temp.first = x;
        temp.second = y;
      }
    } else if(barcos_aux2[y][x] == 'P') {
      auxiliar = ((PortaAvioes *)barcos_player2[y][x])->skill(x, y);
      if (auxiliar == 8) {
        verificar.second = ((PortaAvioes *)barcos_player2[y][x])->to_suffer_damage(x, y);
        if (verificar.second == 2) {
          ((PortaAvioes *)barcos_player2[y][x])->~PortaAvioes();
          scores.first += 8;
          temp.first = x;
          temp.second = y;
        }
      }
    } else if(temp.first == x && temp.second == y){
      cout << "Tiro na água. Você já acertou esta coordenada!" << endl;
    } else if (barcos_aux2[y][x] != 'P' && barcos_aux2[y][x] != 'C' && barcos_aux2[y][x] != 'S'){
      cout << "Tiro na água!" << endl;
    }
  return 0;
}

int Draw::player_2(){
  int x, y, auxiliar;
  pair<int, int> verificar;

  //Resolver o erro de como verificar se as coordenadas que estão sendo atacadas no submarino e no porta aviões já foram atacadas antes
  cout << "----------------------------Turno de " << players.second << "----------------------" << endl;
  cout << "Digite a coordenada x: " << endl;
  cin >> x;
  cout << "\n";
  cout << "Digite a coordenada para y: " << endl;
  cin >> y;
  cout << "\n";

  if (barcos_aux[y][x] == 'C') {
    ((Canoa *)barcos_player1[y][x])->~Canoa();
    barcos_player1[y][x] = NULL;
    scores.second += 2;
  } else if(barcos_aux[y][x] == 'S') {
      verificar.first = ((Submarino *)barcos_player1[y][x])->to_suffer_damage(x, y);
      if (verificar.first == 1) {
        ((Submarino *)barcos_player1[y][x])->~Submarino();
        barcos_player1[y][x] == NULL;
        scores.second += 6;
      }
    } else if(barcos_aux[y][x] == 'P') {
      auxiliar = ((PortaAvioes *)barcos_player1[y][x])->skill(x, y);
      if (auxiliar == 8) {
        verificar.second = ((PortaAvioes *)barcos_player1[y][x])->to_suffer_damage(x, y);
        if (verificar.second == 2) {
          ((PortaAvioes *)barcos_player1[y][x])->~PortaAvioes();
          scores.second += 8;
        }
      }
    } else if (barcos_aux[y][x] != 'P' && barcos_aux[y][x] != 'C' && barcos_aux[y][x] != 'S'){
      cout << "Tiro na água!" << endl;
    }
  return 0;
}

void Draw::tabuleiro_player1(string nome_mapa){
  ifstream mapa_file;
  string linha, tipo, sentido, ignorar;
  int cooX, cooY, linhas, colunas;
  linhas = 13;
  colunas = 13;
  //Vector responsável por armazenar as embarcações
  barcos_player1 = vector<vector<Embarcacao*>>(13, vector<Embarcacao*>(13, NULL));


  mapa_file.open(nome_mapa);

  if (!mapa_file.is_open()) {
    cout << "O arquivo não existe ou não pode ser aberto. Saindo..." << endl;
    exit(1);
    mapa_file.clear();
  }

  barcos_aux = (char **) malloc(linhas * sizeof(char*));
  //Percorre as linhas
    for (int q = 0; q < linhas; q++) {
      barcos_aux[q] = (char *) malloc(colunas * sizeof(char)); //Aloca as colunas
      for (int j = 0; j < colunas; j++) {
        barcos_aux[q][j] = 'x';
      }
    }

  ignorar.clear();
  while (true) {
    getline(mapa_file, ignorar);
    if (ignorar == "# player_1") {
      break;
    }
  }

  for(int j = 0; j < 12; j ++){

    mapa_file >> cooX >> cooY >> tipo >> sentido;

    //Adiciona cada embarcação na coordenada devida
    if (tipo == "canoa") {
      barcos_player1[cooY][cooX] = new Canoa(cooX, cooY);
      barcos_aux[cooY][cooX] = 'C';
    } else if (tipo == "submarino") {
      //barcos[cooY][cooX] = new Submarino(cooX, cooY, 0, 0);
      barcos_aux[cooY][cooX] = 'S';
      if (sentido == "direita") {
        barcos_player1[cooY][cooX] = new Submarino(cooX, cooY, cooX+1, cooY);
        barcos_player1[cooY][cooX+1] = barcos_player1[cooY][cooX];
        barcos_aux[cooY][cooX+1] = 'S';
      } else if (sentido == "baixo") {
        barcos_player1[cooY][cooX] = new Submarino(cooX, cooY, cooX, cooY+1);
        barcos_player1[cooY+1][cooX] = barcos_player1[cooY][cooX];
        barcos_aux[cooY+1][cooX] = 'S';
      } else if (sentido == "esquerda") {
        barcos_player1[cooY][cooX] = new Submarino(cooX, cooY, cooX-1, cooY);
        barcos_player1[cooY][cooX-1] = barcos_player1[cooY][cooX];
        barcos_aux[cooY][cooX-1] = 'S';
      } else if (sentido == "cima") {
        barcos_player1[cooY][cooX] = new Submarino(cooX, cooY, cooX, cooY-1);
        barcos_player1[cooY-1][cooX] = barcos_player1[cooY][cooX];
        barcos_aux[cooY-1][cooX] = 'S';
      }
    } else if (tipo == "porta-avioes") {
        //barcos[cooY][cooX] = new PortaAvioes(cooX, cooY);
        barcos_aux[cooY][cooX] = 'P';
        if (sentido == "direita") {
          barcos_player1[cooY][cooX] = new PortaAvioes(cooX, cooY, cooX+1, cooY, cooX+2, cooY, cooX+3, cooY);
          barcos_player1[cooY][cooX+1] = barcos_player1[cooY][cooX];
          barcos_player1[cooY][cooX+2] = barcos_player1[cooY][cooX];
          barcos_player1[cooY][cooX+3] = barcos_player1[cooY][cooX];
            barcos_aux[cooY][cooX+1] = 'P';
            barcos_aux[cooY][cooX+2] = 'P';
            barcos_aux[cooY][cooX+3] = 'P';
        } else if (sentido == "baixo") {
          barcos_player1[cooY][cooX] = new PortaAvioes(cooX, cooY, cooX, cooY+1, cooX, cooY+2, cooX, cooY+3);
          barcos_player1[cooY+1][cooX] = barcos_player1[cooY][cooX];
          barcos_player1[cooY+2][cooX] = barcos_player1[cooY][cooX];
          barcos_player1[cooY+3][cooX] = barcos_player1[cooY][cooX];
          barcos_aux[cooY+1][cooX] = 'P';
          barcos_aux[cooY+2][cooX] = 'P';
          barcos_aux[cooY+3][cooX] = 'P';
        } else if (sentido == "esquerda") {
          barcos_player1[cooY][cooX] = new PortaAvioes(cooX, cooY, cooX-1, cooY, cooX-2, cooY, cooX-3, cooY);
          barcos_player1[cooY][cooX-1] = barcos_player1[cooY][cooX];
          barcos_player1[cooY][cooX-2] = barcos_player1[cooY][cooX];
          barcos_player1[cooY][cooX-3] = barcos_player1[cooY][cooX];
          barcos_aux[cooY][cooX-1] = 'P';
          barcos_aux[cooY][cooX-2] = 'P';
          barcos_aux[cooY][cooX-3] = 'P';
        } else if (sentido == "cima") {
          barcos_player1[cooY][cooX] = new PortaAvioes(cooX, cooY, cooX, cooY+1, cooX, cooY+2, cooX, cooY+3);
          barcos_player1[cooY+1][cooX] = barcos_player1[cooY][cooX];
          barcos_player1[cooY+2][cooX] = barcos_player1[cooY][cooX];
          barcos_player1[cooY+3][cooX] = barcos_player1[cooY][cooX];
          barcos_aux[cooY+1][cooX] = 'P';
          barcos_aux[cooY+2][cooX] = 'P';
          barcos_aux[cooY+3][cooX] = 'P';
        }
    }

  }

  //Cuidado para não fechá-lo dentro do laço for
  mapa_file.close();
}

void Draw::tabuleiro_player2(string nome_mapa){
  ifstream mapa2_file;
  string linha, tipo, sentido, ignorar;
  int cooX, cooY, linhas, colunas;
  linhas = 13;
  colunas = 13;
  //Vector responsável por armazenar as embarcações
  barcos_player2 = vector<vector<Embarcacao*>>(13, vector<Embarcacao*>(13, NULL));


  mapa2_file.open(nome_mapa);

  if (!mapa2_file.is_open()) {
    cout << "O arquivo não existe ou não pode ser aberto. Saindo..." << endl;
    exit(1);
    mapa2_file.clear();
  }

  barcos_aux2 = (char **) malloc(linhas * sizeof(char*));
  //Percorre as linhas
    for (int q = 0; q < linhas; q++) {
      barcos_aux2[q] = (char *) malloc(colunas * sizeof(char)); //Aloca as colunas
      for (int j = 0; j < colunas; j++) {
        barcos_aux2[q][j] = 'x';
      }
    }

  ignorar.clear();
  while (true) {
    getline(mapa2_file, ignorar);
    if (ignorar == "# player_2") {
      break;
    }
  }

  for(int j = 0; j < 12; j ++){

    mapa2_file >> cooX >> cooY >> tipo >> sentido;

    //Adiciona cada embarcação na coordenada devida
    if (tipo == "canoa") {
      barcos_player2[cooY][cooX] = new Canoa(cooX, cooY);
      barcos_aux2[cooY][cooX] = 'C';
    } else if (tipo == "submarino") {
      //barcos[cooY][cooX] = new Submarino(cooX, cooY, 0, 0);
      barcos_aux2[cooY][cooX] = 'S';
      if (sentido == "direita") {
        barcos_player2[cooY][cooX] = new Submarino(cooX, cooY, cooX+1, cooY);
        barcos_player2[cooY][cooX+1] = barcos_player2[cooY][cooX];
        barcos_aux2[cooY][cooX+1] = 'S';
      } else if (sentido == "baixo") {
        barcos_player2[cooY][cooX] = new Submarino(cooX, cooY, cooX, cooY+1);
        barcos_player2[cooY+1][cooX] = barcos_player2[cooY][cooX];
        barcos_aux2[cooY+1][cooX] = 'S';
      } else if (sentido == "esquerda") {
        barcos_player2[cooY][cooX] = new Submarino(cooX, cooY, cooX-1, cooY);
        barcos_player2[cooY][cooX-1] = barcos_player2[cooY][cooX];
        barcos_aux2[cooY][cooX-1] = 'S';
      } else if (sentido == "cima") {
        barcos_player2[cooY][cooX] = new Submarino(cooX, cooY, cooX, cooY-1);
        barcos_player2[cooY-1][cooX] = barcos_player2[cooY][cooX];
        barcos_aux2[cooY-1][cooX] = 'S';
      }
    } else if (tipo == "porta-avioes") {
        //barcos[cooY][cooX] = new PortaAvioes(cooX, cooY);
        barcos_aux2[cooY][cooX] = 'P';
        if (sentido == "direita") {
          barcos_player2[cooY][cooX] = new PortaAvioes(cooX, cooY, cooX+1, cooY, cooX+2, cooY, cooX+3, cooY);
          barcos_player2[cooY][cooX+1] = barcos_player2[cooY][cooX];
          barcos_player2[cooY][cooX+2] = barcos_player2[cooY][cooX];
          barcos_player2[cooY][cooX+3] = barcos_player2[cooY][cooX];
            barcos_aux2[cooY][cooX+1] = 'P';
            barcos_aux2[cooY][cooX+2] = 'P';
            barcos_aux2[cooY][cooX+3] = 'P';
        } else if (sentido == "baixo") {
          barcos_player2[cooY][cooX] = new PortaAvioes(cooX, cooY, cooX, cooY+1, cooX, cooY+2, cooX, cooY+3);
          barcos_player2[cooY+1][cooX] = barcos_player2[cooY][cooX];
          barcos_player2[cooY+2][cooX] = barcos_player2[cooY][cooX];
          barcos_player2[cooY+3][cooX] = barcos_player2[cooY][cooX];
          barcos_aux2[cooY+1][cooX] = 'P';
          barcos_aux2[cooY+2][cooX] = 'P';
          barcos_aux2[cooY+3][cooX] = 'P';
        } else if (sentido == "esquerda") {
          barcos_player2[cooY][cooX] = new PortaAvioes(cooX, cooY, cooX-1, cooY, cooX-2, cooY, cooX-3, cooY);
          barcos_player2[cooY][cooX-1] = barcos_player2[cooY][cooX];
          barcos_player2[cooY][cooX-2] = barcos_player2[cooY][cooX];
          barcos_player2[cooY][cooX-3] = barcos_player2[cooY][cooX];
          barcos_aux2[cooY][cooX-1] = 'P';
          barcos_aux2[cooY][cooX-2] = 'P';
          barcos_aux2[cooY][cooX-3] = 'P';
        } else if (sentido == "cima") {
          barcos_player2[cooY][cooX] = new PortaAvioes(cooX, cooY, cooX, cooY+1, cooX, cooY+2, cooX, cooY+3);
          barcos_player2[cooY+1][cooX] = barcos_player2[cooY][cooX];
          barcos_player2[cooY+2][cooX] = barcos_player2[cooY][cooX];
          barcos_player2[cooY+3][cooX] = barcos_player2[cooY][cooX];
          barcos_aux2[cooY+1][cooX] = 'P';
          barcos_aux2[cooY+2][cooX] = 'P';
          barcos_aux2[cooY+3][cooX] = 'P';
        }
    }

  }

  //Cuidado para não fechá-lo dentro do laço for
  mapa2_file.close();
}

void Draw::tabuleiro_machine(string nome_mapa){
  ifstream mapa2_file;
  string linha, tipo, sentido, ignorar;
  int cooX, cooY, linhas, colunas;
  linhas = 13;
  colunas = 13;
  //Vector responsável por armazenar as embarcações
  barcos_player2 = vector<vector<Embarcacao*>>(13, vector<Embarcacao*>(13, NULL));


  mapa2_file.open(nome_mapa);

  if (!mapa2_file.is_open()) {
    cout << "O arquivo não existe ou não pode ser aberto. Saindo..." << endl;
    exit(1);
    mapa2_file.clear();
  }

  barcos_aux2 = (char **) malloc(linhas * sizeof(char*));
  //Percorre as linhas
    for (int q = 0; q < linhas; q++) {
      barcos_aux2[q] = (char *) malloc(colunas * sizeof(char)); //Aloca as colunas
      for (int j = 0; j < colunas; j++) {
        barcos_aux2[q][j] = 'x';
      }
    }

  ignorar.clear();
  while (true) {
    getline(mapa2_file, ignorar);
    if (ignorar == "# player_2") {
      break;
    }
  }

  for(int j = 0; j < 12; j ++){

    mapa2_file >> cooX >> cooY >> tipo >> sentido;

    //Adiciona cada embarcação na coordenada devida
    if (tipo == "canoa") {
      barcos_player2[cooY][cooX] = new Canoa(cooX, cooY);
      barcos_aux2[cooY][cooX] = 'C';
    } else if (tipo == "submarino") {
      //barcos[cooY][cooX] = new Submarino(cooX, cooY, 0, 0);
      barcos_aux2[cooY][cooX] = 'S';
      if (sentido == "direita") {
        barcos_player2[cooY][cooX] = new Submarino(cooX, cooY, cooX+1, cooY);
        barcos_player2[cooY][cooX+1] = barcos_player2[cooY][cooX];
        barcos_aux2[cooY][cooX+1] = 'S';
      } else if (sentido == "baixo") {
        barcos_player2[cooY][cooX] = new Submarino(cooX, cooY, cooX, cooY+1);
        barcos_player2[cooY+1][cooX] = barcos_player2[cooY][cooX];
        barcos_aux2[cooY+1][cooX] = 'S';
      } else if (sentido == "esquerda") {
        barcos_player2[cooY][cooX] = new Submarino(cooX, cooY, cooX-1, cooY);
        barcos_player2[cooY][cooX-1] = barcos_player2[cooY][cooX];
        barcos_aux2[cooY][cooX-1] = 'S';
      } else if (sentido == "cima") {
        barcos_player2[cooY][cooX] = new Submarino(cooX, cooY, cooX, cooY-1);
        barcos_player2[cooY-1][cooX] = barcos_player2[cooY][cooX];
        barcos_aux2[cooY-1][cooX] = 'S';
      }
    } else if (tipo == "porta-avioes") {
        //barcos[cooY][cooX] = new PortaAvioes(cooX, cooY);
        barcos_aux2[cooY][cooX] = 'P';
        if (sentido == "direita") {
          barcos_player2[cooY][cooX] = new PortaAvioes(cooX, cooY, cooX+1, cooY, cooX+2, cooY, cooX+3, cooY);
          barcos_player2[cooY][cooX+1] = barcos_player2[cooY][cooX];
          barcos_player2[cooY][cooX+2] = barcos_player2[cooY][cooX];
          barcos_player2[cooY][cooX+3] = barcos_player2[cooY][cooX];
            barcos_aux2[cooY][cooX+1] = 'P';
            barcos_aux2[cooY][cooX+2] = 'P';
            barcos_aux2[cooY][cooX+3] = 'P';
        } else if (sentido == "baixo") {
          barcos_player2[cooY][cooX] = new PortaAvioes(cooX, cooY, cooX, cooY+1, cooX, cooY+2, cooX, cooY+3);
          barcos_player2[cooY+1][cooX] = barcos_player2[cooY][cooX];
          barcos_player2[cooY+2][cooX] = barcos_player2[cooY][cooX];
          barcos_player2[cooY+3][cooX] = barcos_player2[cooY][cooX];
          barcos_aux2[cooY+1][cooX] = 'P';
          barcos_aux2[cooY+2][cooX] = 'P';
          barcos_aux2[cooY+3][cooX] = 'P';
        } else if (sentido == "esquerda") {
          barcos_player2[cooY][cooX] = new PortaAvioes(cooX, cooY, cooX-1, cooY, cooX-2, cooY, cooX-3, cooY);
          barcos_player2[cooY][cooX-1] = barcos_player2[cooY][cooX];
          barcos_player2[cooY][cooX-2] = barcos_player2[cooY][cooX];
          barcos_player2[cooY][cooX-3] = barcos_player2[cooY][cooX];
          barcos_aux2[cooY][cooX-1] = 'P';
          barcos_aux2[cooY][cooX-2] = 'P';
          barcos_aux2[cooY][cooX-3] = 'P';
        } else if (sentido == "cima") {
          barcos_player2[cooY][cooX] = new PortaAvioes(cooX, cooY, cooX, cooY+1, cooX, cooY+2, cooX, cooY+3);
          barcos_player2[cooY+1][cooX] = barcos_player2[cooY][cooX];
          barcos_player2[cooY+2][cooX] = barcos_player2[cooY][cooX];
          barcos_player2[cooY+3][cooX] = barcos_player2[cooY][cooX];
          barcos_aux2[cooY+1][cooX] = 'P';
          barcos_aux2[cooY+2][cooX] = 'P';
          barcos_aux2[cooY+3][cooX] = 'P';
        }
    }

  }

  //Cuidado para não fechá-lo dentro do laço for
  mapa2_file.close();
}

//Inteligência da máquina. Realiza todos os ataques de máquina

void Draw::machine(){
  int maximo, minimo, saida, x, y, auxiliar;
  pair<int, int> verificar;
  maximo = 18; //Valor máximo
  minimo = 0; //Valor mínimo
  saida = rand()%(maximo-minimo+1) + minimo;

  //Resolver o erro de como verificar se as coordenadas que estão sendo atacadas no submarino e no porta aviões já foram atacadas antes
  cout << "----------------------------Turno de " << "Máquina" << "----------------------" << endl;
  x = saida/2;
  y = saida-(saida/3);
  cout << "X: " << x << endl;
  cout << "Y: " << y << endl;

  if (barcos_aux[y][x] == 'C') {
    ((Canoa *)barcos_player1[y][x])->~Canoa();
    barcos_player1[y][x] = NULL;
    scores.second += 2;
  } else if(barcos_aux[y][x] == 'S') {
      verificar.first = ((Submarino *)barcos_player1[y][x])->to_suffer_damage(x, y);
      if (verificar.first == 1) {
        ((Submarino *)barcos_player1[y][x])->~Submarino();
        barcos_player1[y][x] == NULL;
        scores.second += 6;
      }
    } else if(barcos_aux[y][x] == 'P') {
      auxiliar = ((PortaAvioes *)barcos_player1[y][x])->skill(x, y);
      if (auxiliar == 8) {
        verificar.second = ((PortaAvioes *)barcos_player1[y][x])->to_suffer_damage(x, y);
        if (verificar.second == 2) {
          ((PortaAvioes *)barcos_player1[y][x])->~PortaAvioes();
          scores.second += 8;
        }
      }
    } else if (barcos_aux[y][x] != 'P' && barcos_aux[y][x] != 'C' && barcos_aux[y][x] != 'S'){
      cout << "Tiro na água!" << endl;
    }
}
