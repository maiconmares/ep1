#include "submarino.hpp"
#include <iostream>

using namespace std;

Submarino::Submarino(){
  cout << "Submarino adicionado com sucesso na posição" << endl;
}

Submarino::Submarino(int cooXp1, int cooYp1, int cooXp2, int cooYp2){
  //this->cooX = cooX;
  //this->cooY = cooY;
  //this->p1 = make_pair(make_pair(cooX, cooY), 0);
  //this->p2 = make_pair(make_pair(cooXf, cooYf), 0);
  cout << "Um submarino foi adicionado com sucesso!" << endl;
}

Submarino::~Submarino(){
  cout << "Um submarino inimigo foi completamente destruído!" << endl;
}

void Submarino::set_name_emb(string name_emb){
  this->name_emb = name_emb;
}

void Submarino::set_life(int life){
  this->life += life;
}

void Submarino::set_casas(int casas){
  this->casas += casas;
}

int Submarino::to_suffer_damage(int x, int y){
  if ((x != p1.first.first && y != p1.first.second && p1.second == 0) && (x != p2.first.first && y != p2.first.second && p2.second == 0)) {
    cout << "Você acertou uma das casas de um submarino inimigo, mas ainda falta mais um tiro para destruir esta casa!" << endl;
    p1.first.first = x;
    p1.first.second = y;
    p1.second += 1;
  } else if ((x == p1.first.first && y == p1.first.second) && p1.second == 1 && p2.second == 2) {
    cout << "Você destruiu  um submarino inimigo" << endl;
    return 1;
    //((Submarino *)barcos[y][x])->~Submarino();
    //barcos[y][x] = NULL;
    p1.first.first = 0;
    p1.first.second = 0;
    p2.first.first = 0;
    p2.first.second = 0;
    p1.second = 0;
    p2.second = 0;
  } else if((x == p1.first.first && y == p1.first.second) && p1.second == 1 && p2.second < 2) {
    cout << "Você destruiu uma casa de um submarino inimigo, mas ainda falta uma casa para ser destruída" << endl;
    p1.second += 1;
  } else if ((x != p1.first.first || y != p1.first.second) && p1.second >= 2 && p2.second < 1) {
    cout << "Você acertou uma casa de um submarino inimigo, mas ainda falta mais um tiro para destruir esta casa!" << endl;
    p2.first.first = x;
    p2.first.second = y;
    p2.second += 1;
  } else if ((x == p2.first.first && y == p2.first.second) && p2.second == 1) {
    cout << "Você destruiu  um submarino inimigo" << endl;
    p1.first.first = 0;
    p1.first.second = 0;
    p2.first.first = 0;
    p2.first.second = 0;
    p1.second = 0;
    p2.second = 0;
    return 1;
  } else if (((x == p1.first.first && y == p1.first.second) && p1.second == 2) || ((x == p2.first.first && y == p2.first.second) && p2.second == 2)){
    cout << "Tiro na água. Você já acertou esta casa." << endl;
  }
  return 0;
}

//Criar mecanismo que verifica se a mesma casa foi atingida
//Se a mesma casa for atingida chamar to_suffer_damage e passar a casa a ser destruída
void Submarino::skill(){

}
