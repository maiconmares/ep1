#include "porta_avioes.hpp"
#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

PortaAvioes::PortaAvioes(){
  cout << "Porta aviões adicionado na posição" << endl;
}

PortaAvioes::PortaAvioes(int cooXp1, int cooYp1, int cooXp2, int cooYp2, int cooXp3, int cooYp3, int cooXp4, int cooYp4){
  //this->cooX = cooX;
  //this->cooY = cooY;
  p1.first.first = 0;
  p1.first.second = 0;
  p1.second = 0;
  p2.first.first = 0;
  p2.first.second = 0;
  p2.second = 0;
  p3.first.first = 0;
  p3.first.second = 0;
  p3.second = 0;
  p4.first.first = 0;
  p4.first.second = 0;
  p4.second = 0;
  pair< pair<int, int>, int> t1;
  pair< pair<int, int>, int> t2;
  pair< pair<int, int>, int> t3;
  pair< pair<int, int>, int> t4;
  t1 = make_pair(make_pair(cooXp1, cooYp1), 0);
  t2 = make_pair(make_pair(cooXp2, cooYp2), 0);
  t3 = make_pair(make_pair(cooXp3, cooYp3), 0);
  t4 = make_pair(make_pair(cooXp4, cooYp4), 0);
  cout << "Um porta aviões foi adicionado com sucesso!"<<  endl;
}

PortaAvioes::~PortaAvioes(){
    cout << "Um porta aviões inimigo foi totalmente destruído!" << endl;
}

void PortaAvioes::set_name_emb(string name_emb){
  this->name_emb = name_emb;
}

void PortaAvioes::set_life(int life){
  this->life += life;
}

void PortaAvioes::set_casas(int casas){
  this->casas += casas;
}

//Criar mecanismo para saber se a casa da embarcação já foi acertada
int PortaAvioes::to_suffer_damage(int x, int y){
  if ((x != p1.first.first && y != p1.first.second) && p1.second == 0) {
    cout << "Você destruiu uma casa de um porta aviões, mas ainda faltam 3 casas." << endl;
    p1.first.first = x;
    p1.first.second = y;
    p1.second = 1;
  } else if ((x != p1.first.first || y != p1.first.second) && p1.second == 1) {
    cout << "Você destruiu uma casa de um porta aviões, mas ainda faltam 2 casas." << endl;
    p2.first.first = x;
    p2.first.second = y;
    p2.first.second = 1;
    p1.second = 2;
  } else if ((x != p2.first.first || y != p2.first.second) && p1.second == 2){
    cout << "Você destruiu uma casa de um porta aviões, mas ainda faltam 1 casa." << endl;
    p3.first.first = x;
    p3.first.second = y;
    p3.first.second = 1;
    p1.second = 3;
  } else if ((x != p3.first.first || y != p3.first.second) && p1.second == 3){
    p4.first.first = x;
    p4.first.second = y;
    p4.first.second = 1;
    p1.second = 4;
    cout << "Você destruiu um porta aviões inimigo!" << endl;
    return 2;
  } else if ((x == p1.first.first && y == p1.first.second)||(x == p2.first.first && y == p2.first.second)||(x == p3.first.first && y == p3.first.second)||(x == p4.first.first && y == p4.first.second)) {
    cout << "Tiro na água. Você já acertou esta casa!" << endl;
  } else if (p1.second == 4) {
    cout << "Você destruiu um porta aviões inimigo!" << endl;
    return 2;
  }
  return 0;
}

void PortaAvioes::skill(){

}

//Criar mecanismo que passe a casa correta a ser destruída. No caso a mesma casa que o tiro acabou de acertar
//Habilidade de desviar mísseis
int PortaAvioes::skill(int x, int y){
  int maximo, minimo, saida;
  maximo = 1000; //Valor máximo
  minimo = 0; //Valor mínimo
  saida = rand()%(maximo-minimo+1) + minimo;

  /*Tomar cuidado com o srand, pois ele não pode ser chamado mais de uma vez.
  Se ele for chamado mais de uma vez temos a chance de o número gerado não ser
  mais aleatório. Uma solução é chamar este método dentro do construtor de Draw
  ou de outra classe que tem seu construtor chamado somente uma vez.*/

  if (saida >= 600) {
    cout << "O porta aviões abateu o míssel" << endl;
  } else {
    return 8;
  }
  return 0;
}
