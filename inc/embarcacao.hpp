#ifndef EMBARCACAO_HPP
#define EMBARCACAO_HPP
#include <string>
#include <vector>

using namespace std;


//Classe abstrata
class Embarcacao {
  private:
    string name_emb = "";
    int casas, life;
  public:
    //Serão implementados de maneira diferente no cpp de cada classe filha
    //Temos abaixo vários exemplos de sobrecarga de método
    virtual void skill() = 0;
    virtual int to_suffer_damage(int x, int y) = 0;
    virtual void set_name_emb(string name_emb) = 0;
    virtual void set_casas(int casas) = 0;
    virtual void set_life(int life) = 0;
    //vector<vector<Embarcacao>> *barcos;

    /*
    //Pesquisar:
    barcos.clear()
    baros.insert(index, submarino)
    barcos[0];
    barcos.push_back(submarino)

    submarino.x
    submarino.y
    submarino.type


    for(auto obj : barcos)
      if obj.type == dskjfjsdl
      else
        obj.
        */
 };

#endif
