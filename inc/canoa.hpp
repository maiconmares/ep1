#ifndef CANOA_HPP
#define CANOA_HPP

#include <string>
#include "embarcacao.hpp"

class Canoa: public Embarcacao {
  private:
    string name_emb = "";
    int casas, life;
    int cooX, cooY;
  public:
    Canoa();
    Canoa(int cooX, int cooY);
    ~Canoa();
    void set_name_emb(string name_emb);
    int to_suffer_damage(int x, int y);
    void set_casas(int casas);
    void set_life(int life);
    void skill();
};

#endif
