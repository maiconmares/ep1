#ifndef SUBMARINO_HPP
#define SUBMARINO_HPP

#include "embarcacao.hpp"

class Submarino: public Embarcacao{
  private:
    string name_emb = "";
    int casas, life;
    int cooX, cooY;
    pair< pair<int, int>, int> p1;
    pair< pair<int, int>, int> p2;
  public:
    //Construtor sobrecarregado
    Submarino();
    Submarino(int cooXp1, int cooYp1, int cooXp2, int cooYp2);
    ~Submarino();
    void set_name_emb(string name_emb);
    //void to_suffer_damage(int x, int y);
    int to_suffer_damage(int x, int y);
    void set_casas(int casas);
    void set_life(int life);
    void skill();

};

#endif
