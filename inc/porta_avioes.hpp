#ifndef PORTA_AVIOES_HPP
#define PORTA_AVIOES_HPP

#include "embarcacao.hpp"

class PortaAvioes: public Embarcacao {
  private:
    string name_emb = "";
    int casas, life;
    int cooX, cooY;
    pair< pair<int, int>, int> p1;
    pair< pair<int, int>, int> p2;
    pair< pair<int, int>, int> p3;
    pair< pair<int, int>, int> p4;
  public:
    PortaAvioes();
    PortaAvioes(int cooXp1, int cooYp1, int cooXp2, int cooYp2, int cooXp3, int cooYp3, int cooXp4, int cooYp4);
    ~PortaAvioes();
    void set_name_emb(string name_emb);
    int to_suffer_damage(int x, int y);
    void set_casas(int casas);
    void set_life(int life);
    void skill();
    int skill(int x, int y);
};

#endif
