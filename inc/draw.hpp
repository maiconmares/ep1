#ifndef DRAW_HPP
#define DRAW_HPP

#include "porta_avioes.hpp"
#include "submarino.hpp"
#include "canoa.hpp"
#include "embarcacao.hpp"
#include <vector>

class Draw{
  private:
    vector<string> nome_mapas;
    char **barcos_aux;
    char **barcos_aux2;
    vector<vector<Embarcacao*>> barcos_player1;
    vector<vector<Embarcacao*>> barcos_player2;
    pair<int, int>scores;
    pair<string, string>players;
  public:
    Draw();
    ~Draw();
    void initializer(); //Inicializa o jogo
    void menu(); //Responsável pela escolha de opções e delegação de função a cada método de Draw
    void engine(); //Escolhe o mapa que desejamos e posiciona todos os elementos na tela
    //void set_option(int option);
    void show_dir(); //Mostra a lista de mapas disponíveis
    void show_map(int number_map); //Mostra o mapa que desejamos escolher, com embarcações e coordenadas detalhadas
    void organize(); //Lê o arquivo de mapa que escolhemos e separa as coordenadas e embarcacoes no tabuleiro
    int player_1();
    int player_2();
    void player_vs_player();
    void player_vs_machine();
    void tabuleiro_player1(string nome_mapa);
    void tabuleiro_player2(string nome_mapa);
    void tabuleiro_machine(string nome_mapa);
    void machine();
};

#endif
