# EP1 - OO 2019.1 (UnB - Gama)

&nbsp;&nbsp;Este projeto consiste em criar um jogo em C++ similar ao game Battleship (ou batalha naval), onde todas suas instruções, dicas e requisitos para a avaliação estão sendo descritos na [wiki](https://gitlab.com/oofga/eps/eps_2019_1/ep1/wikis/home) do repositório.

## Instruções

&nbsp;&nbsp;Recomenda-se construir o corpo do README com no mínimo no seguinte formato:

* Descrição do projeto
* Instruções de execução

&nbsp;&nbsp;De forma básica, a construção e execução do projeto funciona da seguinte maneira:

Para construir os objetos definidos pelo projeto, juntamente com o binário para realizar a execução:
```
$ make
```

Para executar o binário criado e iniciar o programa:
```
$ make run
```

Caso já tenha, posteriormente a uma mudança, criado os objetos definidos, lembre-se sempre de criar o objeto dos novos, limpando os antigos:
```
$ make clean
```
* Instruções para ler arquivos txt em meu trabalho
Não altere os arquivos txt, pois por algum motivo que não sei se você alterar algum arquivo, mesmo que seja apenas um espaço apagado o meu trabalho fica em loop infinito. Não utilize arquivos txt com embarcações em linhas juntas. Isso também dá problema na hora de alocar dinamicamente, pois não reconhece as coordenadas. 

* Estrutura do jogo
Neste jogo você pode jogar contra um adversário real ou contra a máquina. Uma inteligência simples foi adicionada para jogar contra você. Nunca digite valores fora do intervalo entre 0 e 12 para uma coordenada que você deseja acertar e ou tipos de dados diferentes de números. O tamanho do mapa é estático (13x13).

* Problema ao abrir diretórios e mapas
Em alguns computadores há problema quando o método show_dir() da classe Draw tenta ver os mapas. A solução deve ser feita manualmente. Consiste em alterar o valor da variável name_map = "./doc/", que está na linha 226 de draw.cpp, por "../ep1/doc/". Se o problema persistir o nome do mapa terá que ser substituído manualmente nas linhas 421 e 528. Basta alterar mapa_file.open(nome_mapa); por mapa_file.open("exemplo_mapa.txt"); e mapa2_file.open(nome_mapa); pelo nome do mapa entre parênteses. 

## Observações

* Fique avontade para manter o README da forma que achar melhor, mas é importante manter ao menos as informações descritas acima para, durante a correção, facilitar a avaliação.
* Divirtam-se mas sempre procurando atender aos critérios de avaliação informados na Wiki, ela definirá a nota a ser atribuída ao projeto.
